module.exports = {
  runtimeCompiler: true,
  pages: {
    index: {
      entry: "src/main.js",
      title: "OpenFlexure Microscope STLs",
    },
  },
  chainWebpack: (config) => {
    config.module
      .rule("eslint")
      .use("eslint-loader")
      .tap((opts) => ({ ...opts, emitWarning: true }));
  },
};
