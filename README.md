# [microscope-stls.openflexure.org](https://microscope-stls.openflexure.org)

> STL file selector for the OpenFlexure Microscope

## Project setup
```
npm install
```

## Pre-computation

Copy an updated `stl_options.json` (obtained from the the Openflexure build process) into the root of this folder and run `./pre_compute`. 


By default this script spawns 3 processes. If that's a problem, or you want to try to speed it up and have more than 2 computer cores you can change it by specifying a number:

```
./pre_compute 1 # runs 1 process
./pre_compute 9 # runs 9 processes in parallel
```


### Compiles and hot-reloads for development
```
npm run serve
```

Vist http://localhost:8080 to see the dev site. 

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize Vue configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
